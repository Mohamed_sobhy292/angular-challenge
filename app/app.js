'use strict';




/* APP ROUTING */

angular
 .module('myApp', ['ngRoute','ui.router'])
 .config(function($stateProvider,$urlRouterProvider) {
  $urlRouterProvider.otherwise('/');
  $stateProvider
  .state('home', {
    url: '/',
    templateUrl: 'home/home.view.html',
    controller: 'homeCtrl as home'
  })
  .state('users', {
    url: '/users',
    templateUrl: 'users/users.view.html',
    controller: 'usersCtrl as users',
    resolve: {
        parameters: function($stateParams){
            return $stateParams;
        }
    }
  })
  .state('about', {
    url: '/about',
    templateUrl: 'about/about.view.html',
    controller: 'aboutCtrl as about'
  })
  .state('singleUser', {
    url: '/user/{login}',
    templateUrl: 'users/users.view.html',
    controller: 'usersCtrl as users',
    resolve: {
        parameters: function($stateParams){
            return $stateParams;
        }
    }

  });
});
