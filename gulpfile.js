var gulp = require('gulp');
const minifyCss = require('gulp-clean-css'); // MINIFY CSS
const environments = require('gulp-environments'); // SET ENVIRONMENT TO DEVELOPMENT OR PRODUCTION
var nunjucks = require('gulp-nunjucks-render'); // RENDER NUNJUCKS TEMPLATES
var sass = require('gulp-sass');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');
var uglify = require('gulp-uglify'); // MINIFY JS
var concat = require('gulp-concat'); // MINIFY JS
var autoPrefixer = require('gulp-autoprefixer');
var changed = require('gulp-changed');
const del = require('del'); // DELETE FILES
var htmlmin = require('gulp-htmlmin');
const gulpif = require('gulp-if');
var imagemin = require('gulp-imagemin');
var plumber = require('gulp-plumber');
var useref = require('gulp-useref'); // CONCATINATE (JS,CSS) FILES
var faltten = require('gulp-flatten'); // REMOVE OR REPLACE RELATIVE PATH FOR FILES
var runSequence = require('run-sequence');
var wiredep = require('wiredep').stream; // AUTO INJECT BOWER COMPONENTS TO THE HTML
var ngAnnotate = require('gulp-ng-annotate');
var sourcemaps = require('gulp-sourcemaps');

var svgmin = require('gulp-util'); // gulp-svgstore depends on it
var svgstore = require('gulp-svgstore'); // COMBINE SVG FILES INTO ONE WITH ELEMENTS
var svgmin = require('gulp-svgmin'); // MINIFY SVG AND ENSURE UNIQUE IDS.
var size = require('gulp-size'); // OUTPUTS THE SIZE OF VARIOUS FILES FOR THE USER
var surge = require('gulp-surge');

var development = environments.development;
var production = environments.production;
var reload = browserSync.reload;
var dev_path = "app";
var tmp_path = "app";
var build_path = "dist";

/* LIVE RELOAD */
gulp.task('browserSync', function () {
    browserSync.init({
        server: {
            baseDir: dev_path,
            routes: {
                '/bower_components': 'bower_components'
            }
        }
    });
    // START WATCHING FILES
    // gulp.watch(dev_path + '**/*.html', ['refreshHtml']);
    gulp.watch(dev_path + '/assets/styles/**/*.scss', ['sass']);
    gulp.watch(dev_path + '/assets/styles/**/**/*.scss', ['sass']);
    gulp.watch([
        dev_path + '/**/*.html',
        dev_path + '/**/*.js'
    ]).on('change', reload);
});

gulp.task('clean-build', function () { //CLEAN "dist" FOLDER BEFORE BUILD);
    return del([build_path + '/*']);
});

gulp.task('clean-tmp', function () { //CLEAN "dist" FOLDER BEFORE BUILD);
    return del([tmp_path + '/*']);
});

gulp.task('cleanjs', function () { //CLEAN "dist" FOLDER BEFORE BUILD);
    return del([tmp_path + '/scripts/']);
});

gulp.task('cleanimgs', function () {

    return development(del([tmp_path + '/images/**/*']));
});

gulp.task('copyfonts', function () {
    return gulp.src(['bower_components/**/*.{eot,svg,ttf,woff,woff2}', dev_path+'/assets/fonts/*'])
        .pipe(faltten())
        .pipe(production(gulp.dest(build_path +'/assets/fonts/')));
});

gulp.task('copyimgs', ['cleanimgs'], function () {
    return gulp.src([dev_path + '/assets/images/**/*'])
        .pipe(plumber())
        .pipe(development(gulp.dest(tmp_path + '/assets/images/')))
        .pipe(development(reload({
            stream: true
        })))
        .pipe(production((imagemin())))
        .pipe(production(gulp.dest(build_path + '/assets/images/')));
});

gulp.task('cleanHtml', function () {
    return development(del([tmp_path + '/partials/*', tmp_path + '/*.html']));
});

gulp.task('copyjs', function () {
    return gulp.src(dev_path + '/scripts/*.js')
        .pipe(plumber())
        .pipe(development(changed(tmp_path + '/scripts/')))
        .pipe(development(gulp.dest(tmp_path + '/scripts/')))
        .pipe(production(gulp.dest(build_path + '/scripts/')));
});


gulp.task('env_dev', function () { //SET DEVELOPMENT TO PREVENT MINIFICATION
    return environments.current(development);
});

gulp.task('env_prod', function () {
    return environments.current(production);
});

gulp.task('htmlmin', function () {
    return gulp.src(build_path + '/*.html')
        .pipe(plumber())
        .pipe(htmlmin({
            collapseWhitespace: true
        }))
        .pipe(gulp.dest(build_path))
});

gulp.task('copyjson',function() {
    return gulp.src(dev_path + '/**/*.json')
    .pipe(plumber())
    .pipe(wiredep())
    .pipe(gulp.dest(build_path))
})


gulp.task('min-imgs', function () {
    return gulp.src([tmp_path + '/assets/images/*',tmp_path + '/assets/images/**/*'])
        .pipe(plumber())
        //.pipe(imagemin())
        .pipe(gulp.dest(build_path + '/assets/images/'));
});

gulp.task('refreshHtml', function () { //RENDER HTML
    return gulp.src(dev_path + '/**/*.html')
        .pipe(plumber())
        .pipe(production(useref())) //CONCATENATE JS / CSS FILES AND MINIFY THEM
        .pipe(gulpif('*.js', ngAnnotate()))
        .pipe(production(gulpif('*.js', uglify())))
        //.pipe(production(gulpif('*.css', minifyCss())))
        // .pipe(production(gulpif('*.html', htmlmin({collapseWhitespace: true}))))
        .pipe(development(reload({
            stream: true
        })))
        .pipe(production(gulp.dest(build_path)));
});

gulp.task('sass', function () { //CONVERT SASS INTO CSS AND MINIFY IT
    return gulp.src(dev_path + '/assets/styles/*.scss')
    .pipe(sourcemaps.init())
        .pipe(plumber())
        .pipe(sass({errLogToConsole: true}))
        .pipe(sass())
        .pipe(changed(dev_path + '/assets/style/', {
            extension: '.css'
        }))
        .pipe(autoPrefixer({
            browsers: ['> 0%'],
            remove: false
        }))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(dev_path + '/assets/styles/'))
        .pipe(browserSync.stream());

});

gulp.task('svgstore', function () {
    return gulp.src(dev_path + '/assets/images/svgs/icons/*')
        .pipe(plumber())
        .pipe(svgmin(function (file) {
            return {
                plugins: [{
                    cleanupIDs: {
                        minify: true
                    }
                }]
            }
        }))
        .pipe(svgstore())
        .pipe(size())
        .pipe(rename('icons-set.svg'))
        .pipe(gulp.dest(dev_path + '/assets/images/svgs/'));
});

gulp.task('deploy', [], function () {
  return surge({
    project: './dist',
    domain: 'trendak-demo.surge.sh'
  })
})

gulp.task('serve', function () {
    runSequence('env_dev', 'sass', 'svgstore', 'browserSync');
});

gulp.task('build', function () {
    runSequence('clean-build', 'env_prod', 'refreshHtml',  'htmlmin' ,  'copyjs','copyjson', ['min-imgs', 'copyfonts']);
});
